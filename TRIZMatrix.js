//┌──────────────────────────────────────────────────────────────────────┐
//│ Global Live Variables                                                │
//└──────────────────────────────────────────────────────────────────────┘

let atrDic = {};                                // Attributes dictionary
let prnDic = {};                                // Principles dictionary
let comDic = {};                                // Combinations dictionary
let rnkDic = {};                                // Ranking dictionary
let ponArr = [];                                // Ponderation array

//┌──────────────────────────────────────────────────────────────────────┐
//│ General-Purpose Functions                                            │
//└──────────────────────────────────────────────────────────────────────┘
//──Functions to filter only numerical inputs─────────────────────────────
function setInputFilter(box){                   // Filter setup function
  [                                             // Possible triggers array
    'input',                                    // Input change event
    'keydown',                                  // Keystroke trigger
    'keyup',                                    // Keystroke release
    'mousedown',                                // Mouse click trigger
    'mouseup',                                  // Mouse click release
    'select',                                   // Content selection
    'contextmenu',                              // Right click trigger
    'drop',                                     // Element dragging
    'focusout'                                  // Click outside the box
  ]                                             // End of the array
  .forEach(x => filterChecker(x, box));         // Evaluate per element
}                                               // End of the function

function filterChecker(x, box){                 // Filter processing logic
  box.addEventListener(                         // Listener functionality
    x,                                          // Event type trigger
    function(x){                                // Blind function opening
      if(floatRegex(this.value)){               // If regex filter pass
        if(                                     // If there is an input
          ['keydown', 'mousedown', 'focusout']  // Any input event
          .indexOf(x.type) >= 0                 // Confirm new input
        ){                                      // End of the conditions
          this.classList.remove('inpError');    // Remove CSS error style
          this.setCustomValidity('');           // No error message
        }                                       // End of the subcondition
        this.oldValue =                         // Old value registry
          this.value;                           // Update value
        this.oldSelectionStart =                // Old selection start
          this.selectionStart;                  // Pointer update
        this.oldSelectionEnd =                  // Old selection end
          this.selectionEnd;                    // Pointer update
      }                                         // End of the case
      else if(                                  // If regex filter fail
        this.hasOwnProperty('oldValue')         // Verify registry
      ){                                        // End of the conditions
        this.classList.add('inpError');         // Call CSS error style
        this.setCustomValidity(                 // Set error message
          'Only numbers are valid'              // Error message
        );                                      // End of the setup
        this.reportValidity();                  // Fire error label
        this.value = this.oldValue;             // Forbid contents
        this.setSelectionRange(                 // Set selection range
          this.oldSelectionStart,               // Start pointer rollback
          this.oldSelectionEnd                  // End pointer rollback
        );                                      // End of the setup
      }                                         // End of the case
      else{                                     // First attempt safeguard
        this.value = '';                        // Empty contents
      }                                         // End of the conditional
    }                                           // End of the subfunction
  );                                            // End of the setup
}                                               // End of the function

function floatRegex(x){                         // Regex float filter
  return /^-?\d*[.,]?\d*$/.test(x);             // Evaluate regex
}                                               // End of the function

//┌──────────────────────────────────────────────────────────────────────┐
//│ XML Functionality                                                    │
//└──────────────────────────────────────────────────────────────────────┘
//──XML parsing and assignment────────────────────────────────────────────
function xmlAssignment(file){                   // XML assignment function
  let list = file.responseXML;                  // XML document loading
  let atrObj =                                  // Attributes object
    list.getElementsByTagName('attribute');     // Fetch objects by tag
  let prnObj =                                  // Principles object
    list.getElementsByTagName('principle');     // Fetch objects by tag
  let comObj =                                  // Combinations object
    list.getElementsByTagName('combo');         // Fetch objects by tag
  for(let i = 0; i < atrObj.length; i++){       // Loop through attributes
    let cId = atrObj[i]                         // Id holding variable
      .getElementsByTagName('code')[0]          // Fetch object by tag
      .childNodes[0].nodeValue;                 // Extract object's data
    let cNm = atrObj[i]                         // Name holding variable
      .getElementsByTagName('name')[0]          // Fetch object by tag
      .childNodes[0].nodeValue;                 // Extract object's data
    atrDic[cId] = cNm;                          // Add dictionary entry
  }                                             // End of the loop
  for(let i = 0; i < prnObj.length; i++){       // Loop through principles
    let cId = prnObj[i]                         // Id holding variable
      .getElementsByTagName('code')[0]          // Fetch object by tag
      .childNodes[0].nodeValue;                 // Extract object's data
    let cNm = prnObj[i]                         // Name holding variable
      .getElementsByTagName('name')[0]          // Fetch object by tag
      .childNodes[0].nodeValue;                 // Extract object's data
    let cDs = prnObj[i]                         // Description variable
      .getElementsByTagName('description')[0]   // Fetch object by tag
      .childNodes[0].nodeValue;                 // Extract object's data
    prnDic[cId] = [cNm, cDs, 0];                // Add dictionary entry
  }                                             // End of the loop
  for(let i = 0; i < comObj.length; i++){       // Loop through combos
    let cel = [];                               // Define empty array
    for(let j = 0; j < 4; j++){                 // Loop through four cells
      let val =                                 // Temporal container
        comObj[i]                               // Open current index
        .getElementsByTagName('value')[j]       // Fetch object by tag
        .childNodes[0].nodeValue;               // Extract object's data
      if(val == 'NA'){                          // If the value is empty
        val = '';                               // Convert to empty string
        cel.push(val);                          // Push to target array
      }                                         // End of the case
      else{                                     // If there are contents
        cel.push(Number(val));                  // Push converted value
      }                                         // End of the conditional
    }                                           // End of the 2nd loop
    comDic[comObj[i].getAttribute('id')] = cel; // Add dictionary entry
  }                                             // End of the 1st loop
}                                               // End of the function

//──XML document loading──────────────────────────────────────────────────
function xmlLoading(){                          // XML loader function
  atrDic = {};                                  // Cleansing attributes
  prnDic = {};                                  // Cleansing principles
  comDic = {};                                  // Cleansing combinations
  let doc = new XMLHttpRequest();               // Define a new request
  doc.onreadystatechange = function(){          // Function pass on change
    if(                                         // Start a conditional
      this.readyState == 4 &&                   // Check for "Done" state
      this.status == 200                        // Check if "Successful"
    ){                                          // End of the conditions
      xmlAssignment(this);                      // Begin data assignment
    }                                           // End of the conditional
  };                                            // End of the passing
  doc.open('GET', 'database.xml', true);        // Request retrieving file
  doc.send();                                   // Submit the request
}                                               // End of the function

//┌──────────────────────────────────────────────────────────────────────┐
//│ HTML Functionality                                                   │
//└──────────────────────────────────────────────────────────────────────┘
//──Mode changing functions───────────────────────────────────────────────
function selParsing(){                          // Selector write function
  const tot = Object.keys(atrDic).length + 1;   // Dictionary total length
  let sel = '';                                 // Output variable
  for(let i = 1; i < tot; i++){                 // Loop through attributes
    sel += '<option value="';                   // HTML option opening
    sel += i.toString() + '" onmouseover="';    // Option value
    sel += 'showAttribute(this);" aname="';     // Option functionality
    sel += atrDic[i] + '">';                    // Option name label
    sel += i.toString() + ' ― ' + atrDic[i];    // Option display name
    sel += '</option>\n';                       // HTML option closing
  }                                             // End of the loop
  return sel;                                   // Selector to print
}                                               // End of the function

function modeChange(){                          // Mode changing function
  const chk =                                   // Checking variable
    document.querySelectorAll(                  // HTML objects query
      'input[name="modeSel"]:checked'           // Search name and check
    )[0]                                        // Select only instance
    .value                                      // Value extraction
  let tab = '<table>\n<tr><td>';                // Output variable
  switch(chk){                                  // Switch statements
    case 'R':                                   // Regular case
      tab += 'Regular mode</td></tr>\n';        // Header contents
      tab += '<tr><td>\n<select name="regTriz"';// HTML selector opening
      tab += ' id="regTriz" onmouseout="';      // Selector Id
      tab += 'clearAttribute();" class="box" '; // Selector functionality
      tab += 'multiple>\n';                     // Selector header end
      tab += selParsing();                      // Selector options
      tab += '</select>\n</td></tr>\n</table>'; // HTML selector closing
      varMenu.innerHTML = tab;                  // Table to print
    break;                                      // End of the case
    case 'I':                                   // Irregular case
      let opt = selParsing();                   // Options generation
      tab += 'Improving</td><td>';              // First header contents
      tab += 'Worsening</td></tr>\n';           // Second header contents
      tab += '<tr><td>\n<select name="impTriz"';// HTML selector opening
      tab += ' id="impTriz" onmouseout="';      // First selector Id
      tab += 'clearAttribute();" class="box" '; // Selector functionality
      tab += 'multiple>\n';                     // Selector header end
      tab += opt;                               // Selector options
      tab += '</select>\n</td><td>\n<select ';  // HTML selector closing
      tab += 'name="worTriz" id="worTriz" ';    // Second selector Id
      tab += 'onmouseout="clearAttribute();" '; // Selector functionality
      tab += 'class="box" multiple>\n';         // Selector header end
      tab += opt;                               // Selector options
      tab += '</select>\n</td></tr>\n</table>'; // HTML selector closing
      varMenu.innerHTML = tab;                  // Table to print
    break;                                      // End of the case
  }                                             // End of the switches
}                                               // End of the function

//──Attribute displaying functions────────────────────────────────────────
function clearAttribute(){                      // Cleansing function
  attribureName.innerHTML = '';                 // Clearing target
}                                               // End of the function

function showAttribute(sel){                    // Displaying function
  attribureName.innerHTML =                     // Writing target
    sel.getAttribute('aname');                  // Variable printing
}                                               // End of the function

//──TRIZ matrix generation functions──────────────────────────────────────
function celContents(key){                      // Partial cells function
  let vecInp = comDic[key];                     // Corresponding array
  let vecOut = [];                              // Output variable
  for(let i = 0; i < vecInp.length; i++){       // Loop through the array
    if(vecInp[i] == ''){                        // If the value is empty
      vecOut.push('');                          // Push to target array
    }                                           // End of the case
    else{                                       // If there are contents
      vecOut.push(                              // Push to target array
        '<abbr title="' + prnDic[vecInp[i]][0] +// Pop-up title
        ':\n' + prnDic[vecInp[i]][1] + '">' +   // Pop-up contents
        vecInp[i] + '</abbr>'                   // Cell contents tag
      );                                        // End of the push
      rnkDic[vecInp[i]][2] += ponArr[i];        // Ranking update
    }                                           // End of the conditional
  }                                             // End of the loop
  return vecOut;                                // Array to output
}                                               // End of the function

function tabContents(x, y){                     // Partial matrix function
  let inpId = 'I' + x + 'W' + y;                // Generating key value
  let vec = celContents(inpId);                 // Table contents array
  let tab = '<table class="conCl">\n<tr><td>';  // HTML table opening
  tab += vec[0] + '</td><td>';                  // Cell 1-1 contents
  tab += vec[1] + '</td></tr><tr><td>';         // Cell 1-2 contents
  tab += vec[2] + '</td><td>';                  // Cell 2-1 contents
  tab += vec[3] + '</td></tr>\n';               // Cell 2-2 contents
  tab += '</table>';                            // HTML table closing
  return tab;                                   // Table to print
}                                               // End of the function

function tableGenerator(){                      // Table generator function
  let vIm = [];                                 // Improving values array
  let vWr = [];                                 // Worsening values array
  const chk =                                   // Checking variable
    document.querySelectorAll(                  // HTML objects query
      'input[name="modeSel"]:checked'           // Search name and check
    )[0]                                        // Select only instance
    .value                                      // Value extraction
  if(chk == 'R'){                               // If regular case
    vIm =                                       // First input array
      Array.from(                               // Array conversion
        regTriz.selectedOptions                 // Fetch selected options
      )                                         // End of the conversion
      .map(({value}) => value);                 // Operation per element
    vWr =                                       // Second input array
      Array.from(                               // Array conversion
        regTriz.selectedOptions                 // Fetch selected options
      )                                         // End of the conversion
      .map(({value}) => value);                 // Operation per element
  }                                             // End of the case
  else if(chk == 'I'){                          // If irregular case
    vIm =                                       // First input array
      Array.from(                               // Array conversion
        impTriz.selectedOptions                 // Fetch selected options
      )                                         // End of the conversion
      .map(({value}) => value);                 // Operation per element
    vWr =                                       // Second input array
      Array.from(                               // Array conversion
        worTriz.selectedOptions                 // Fetch selected options
      )                                         // End of the conversion
      .map(({value}) => value);                 // Operation per element
  }                                             // End of the conditional
  rnkDic = structuredClone(prnDic);             // Copy object contents
  ponArr = [];                                  // Cleansing ponderation
  for(let i = 1; i < 5; i++){                   // Loop thought inputs
    ponArr.push(                                // Push to target array
      Number(                                   // Number conversion
        document.getElementById('pon' + i)      // Variable fetching
        .value                                  // Value extraction
      )                                         // End of the conversion
    );                                          // End of the push
  }                                             // End of the loop
  let tab =                                     // Output matrix variable
    '<table class="trizMtx">\n';                // HTML table opening
  for(let i = 0; i < vIm.length + 1; i++){      // Loop through rows
    tab += '<tr>';                              // HTML row opening
    for(let j = 0; j < vWr.length + 1; j++){    // Loop through columns
      if(i == 0 && j == 0){                     // If first cell
        tab += '<td class="hedCr">\n';          // Header box cell start
        tab += '<table class="hedCl">\n';       // HTML table opening
        tab += '<tr><td></td><td>W</td></tr>';  // First row contents
        tab += '<tr><td>I</td><td></td></tr>\n';// Second row contents
        tab += '</table>\n</td>';               // HTML table closing
      }                                         // End of the case
      else if(i == 0){                          // If column header
        tab += '<td class="worCl">';            // Column header class
        tab += '<abbr title="';                 // Pop-up header
        tab += atrDic[vWr[j-1]] + '">';         // Pop-up contents
        tab += vWr[j-1] + '</abbr></td>';       // Cell header contents
      }                                         // End of the case
      else if(j == 0){                          // If row header
        tab += '<td class="impCl">';            // Row header class
        tab += '<abbr title="';                 // Pop-up header
        tab += atrDic[vIm[i-1]] + '">';         // Pop-up contents
        tab += vIm[i-1] + '</abbr></td>';       // Cell header contents
      }                                         // End of the case
      else if(vIm[i-1] == vWr[j-1]){            // If in the diagonal
        tab += '<td class="digCl"></td>';       // Diagonal cell contents
      }                                         // End of the case
      else{                                     // If regular cell
        tab += '<td>';                          // HTML column opening
        tab += tabContents(vIm[i-1], vWr[j-1]); // Cell default content
        tab += '</td>';                         // HTML column closing
      }                                         // End of the conditional
    }                                           // End of the 2nd loop
    tab += '</tr>\n';                           // HTML row closing
  }                                             // End of the 1st loop
  tab += '</table>';                            // HTML table closing
  let ran =                                     // Output ranking variable
    '<table>\n';                                // HTML table opening
  let ranArr =                                  // Ordered ranking array
    Object.entries(rnkDic)                      // Convert object to array
    .sort(([, a], [, b]) => b[2] - a[2]);       // Sort by nested value
  for(let i = 0; i < ranArr.length; i++){       // Loop through ranks
    ran += '<tr><td>' + ranArr[i][0] + ': ';    // Element number
    ran += ranArr[i][1][0] + ' - ';             // Element name
    ran += ranArr[i][1][2] + '</td></tr>';      // Element ranking
  }                                             // End of the loop
  ran += '</table>';                            // HTML table closing
  cMatrix.innerHTML = tab;                      // Matrix to print
  wRank.innerHTML = ran;                        // Ranking to print
}                                               // End of the function

//┌──────────────────────────────────────────────────────────────────────┐
//│ Document Event Listeners                                             │
//└──────────────────────────────────────────────────────────────────────┘
//──General execution function────────────────────────────────────────────
function domLoading(){                          // Invoking function
  xmlLoading();                                 // Load XML contents
  for(let i = 1; i < 5; i++){                   // Loop thought inputs
    setInputFilter(                             // Assign filter per input
      document.getElementById('pon' + i)        // Object fetching
    );                                          // End of assignment
  }                                             // End of the loop
}                                               // End of the function

//──Execution activation at DOM loading────────────────────────────────────
document.addEventListener(                      // Add document listener
  'DOMContentLoaded', domLoading                // Execute after DOM load
);                                              // End of passing